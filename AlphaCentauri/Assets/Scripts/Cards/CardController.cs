﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardController : MonoBehaviour {

    private static List<CardController> nodes;
    private MeshRenderer meshRenderer;
    
    
    public List<(CardController,LineRenderer)> links;
    public Card card;

    private static CardController firstCard;
    
    
    public void Init(float maxDistance) {
        links = new List<(CardController, LineRenderer)>();
        meshRenderer = GetComponent<MeshRenderer>();
        if (nodes == null) {
            nodes = new List<CardController>();
            firstCard = this;
        }

        List<CardController> tmpNeighbours = nodes.FindAll(c => Vector3.Distance(c.transform.position, transform.position) < maxDistance);

        foreach (CardController neighbour in tmpNeighbours) {
            
            neighbour.links.Add((this,AddLineRender(neighbour)));
            links.Add((neighbour , neighbour.links.Find(c => c.Item1 == this).Item2));
        }

        
        nodes.Add(this);

    }


    // Update is called once per frame
    public void Explore()
    {
        if (card is Encounter)
        {
            ((Encounter)card).Flip();
        }
    }

    public CardController GetFirstCard()
    {
        return firstCard;
    }


    private LineRenderer AddLineRender(CardController cardController) {
        
        var child = new GameObject();
        child.transform.parent = transform;
        child.transform.localPosition = Vector3.zero;
            
        var l = child.AddComponent<LineRenderer>();
        l.positionCount = 2;
        l.SetPositions( new []{ transform.position, cardController.transform.position } );
        l.startColor = new Color(1,1f,1f);
        l.endColor = l.startColor;
        l.startWidth = 0.02f;
        l.endWidth = l.startWidth;
        l.material = new Material(Shader.Find("Sprites/Default"));
        return l;
    }


    public void TurnOn() {
        meshRenderer.material.color = Color.green;
    }

    public void Pause() {
        meshRenderer.material.color = Color.yellow;
    }

    public void TurnOff() {
        meshRenderer.material.color = Color.white;
        
    }
    public void TurnOnNeighbours() {
        RecolorLinks(Color.green);
    }
    public void TurnOffNeighbours() {
        RecolorLinks(Color.white);
    }
    public void PauseNeighbours() {
        RecolorLinks(Color.yellow);
    }

    public void RecolorLinks(Color color) {
        foreach ((CardController,LineRenderer) link in links) {
            link.Item2.startColor = color;
            link.Item2.endColor =color;
            link.Item1.meshRenderer.material.color = color;

            LineRenderer lineRenderer = link.Item1.links.Find(c => c.Item1 == this).Item2;

            if (lineRenderer != null) {
                lineRenderer.startColor = color;
                lineRenderer.endColor = color;
            }
        }
    }

}
