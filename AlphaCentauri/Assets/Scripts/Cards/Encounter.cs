﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Encounter", menuName = "ScriptableObjects/Encounter")]
public class Encounter : Card {
    public Choice[] choices;
    public RiskManager riskManager;

    private void Awake()
    {
        
    }

    public void Flip()
    {
        if (riskManager == null) {
            riskManager = FindObjectOfType<RiskManager>();
        }

        riskManager.StartEncounter(choices);
    }
    
    
}
