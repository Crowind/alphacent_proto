﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDeck", menuName = "ScriptableObjects/Deck", order = 0)]
public class Deck : ScriptableObject
{
	public List<Card> cardList = new List<Card>();
}
