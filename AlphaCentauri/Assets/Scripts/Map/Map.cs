using System;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(BoxCollider2D))]
public class Map : MonoBehaviour {

	[SerializeField] private Deck deck;
	[SerializeField] private GameObject cardControllerPrefab;
	[SerializeField] private float minDeltaRandom;
	[SerializeField] private float maxDeltaRandom;
	
	[SerializeField] private BoxCollider2D spawnArea;

	private void Awake() {
		GenerateMap();
	}

	public void GenerateMap() {
		int count = deck.cardList.Count;

		int sections = Mathf.CeilToInt(Mathf.Sqrt(count));
		
		Vector3 min = spawnArea.bounds.min;
		// ReSharper disable once Unity.InefficientPropertyAccess
		Vector3 max = spawnArea.bounds.max;

		float dx = (max.x - min.x) / sections;
		float dy = (max.y - min.y) / sections;

		for (int i = 0; i < deck.cardList.Count; i++) {
			Card card = deck.cardList[i];
			
			
			var delta = new Vector3(Random.Range(0f,1f),Random.Range(0f,1f),0  );
			delta = delta.normalized * Random.Range(minDeltaRandom, maxDeltaRandom);
			Vector3 position =  min + new Vector3( dx * (0.5f+(i % sections)),dy * (0.5f + (int)(i/sections)),0) + delta;
			
			GameObject obj = Instantiate(cardControllerPrefab,position, Quaternion.identity,transform);
			obj.GetComponent<CardController>().Init((maxDeltaRandom -minDeltaRandom)/5+  new Vector3(dx,dy).magnitude);
			obj.GetComponent<CardController>().card = card;
		}
	}

	private void OnDrawGizmos() {
		Gizmos.color = Color.magenta;
		Vector3 startPoint = spawnArea.bounds.min + (Vector3)Vector2.one * 0.02f;
		Gizmos.DrawLine(startPoint,startPoint + Vector3.right * maxDeltaRandom );
		Gizmos.color = Color.yellow;
		startPoint += Vector3.up * 0.02f;
		Gizmos.DrawLine(startPoint,startPoint + Vector3.right * minDeltaRandom );
	}
}