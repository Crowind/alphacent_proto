using System;
using UnityEngine;

[CreateAssetMenu(fileName = "Effect", menuName = "ScriptableObjects/Effect")]
public abstract class Effect : ScriptableObject {

	public string effectName;

	public virtual void Apply() {
		
		throw new NotImplementedException();
	}

}

