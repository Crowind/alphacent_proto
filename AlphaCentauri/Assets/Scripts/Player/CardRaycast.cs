﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static  class CardRaycast 
{
	private static RaycastHit hit;
	private static int layerMask = 1 << 8;
	
	public static CardController CheckCard()
	{	
		if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit,Mathf.Infinity ,layerMask))
		{
			return hit.collider.GetComponent<CardController>();
		}
		return null;
	}
	
	
}
