﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character", menuName = "ScriptableObjects/Character")]
public class Character : ScriptableObject {
	private Brain brain;
	public Equipment[] equip;
	public Ability[] abilities;
	public Trait[] traits;
	public int healthPoints;
}
