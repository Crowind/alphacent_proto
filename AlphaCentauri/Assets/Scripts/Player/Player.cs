﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class Player : MonoBehaviour
{
	public int fuel = 100;
	public CardController currentPosition;

	public bool canMove = true;
	
	private void Start()
	{
		currentPosition = FindObjectOfType<CardController>().GetFirstCard();
		transform.position = currentPosition.transform.position - Vector3.forward;
		currentPosition.TurnOn();
		currentPosition.TurnOnNeighbours();

	}

	private void Update()
	{
		CardController cardChecked = CardRaycast.CheckCard();
		
		if (canMove && Input.GetKeyDown(KeyCode.Mouse0) && cardChecked)
		{
			foreach ((CardController,LineRenderer) currentLink in currentPosition.links)
			{
				if (currentLink.Item1 == cardChecked) {
					
					
					currentPosition.TurnOff();
					currentPosition.TurnOffNeighbours();
					
					Vector3 newPos = cardChecked.transform.position;
					transform.position =newPos - Vector3.forward;
					currentPosition = cardChecked;
					currentPosition.Explore();
					currentPosition.TurnOn();
					currentPosition.TurnOnNeighbours();
				}
			}
		}
		else if (canMove) {
			currentPosition.TurnOn();
			currentPosition.TurnOnNeighbours();
		}
		if (!canMove) {
				currentPosition.Pause();
				currentPosition.PauseNeighbours();
			
		}
		
	}
}
