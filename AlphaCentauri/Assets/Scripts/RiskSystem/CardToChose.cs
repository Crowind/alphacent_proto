using System;
using System.Collections.Generic;
using UnityEngine;

public class CardToChose : MonoBehaviour {

	public Choice.Outcome outcome;
	private RiskManager riskManager;

	private void Start() {
		riskManager = FindObjectOfType<RiskManager>();
	}

	private void Update() {


		if (Input.GetKeyDown(KeyCode.Mouse0)) {
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit info, Mathf.Infinity, 1)) {

				var tmpCard = info.collider.gameObject.GetComponent<CardToChose>();

				if (tmpCard && tmpCard == this) {
					riskManager.Pick(outcome);
				}
			}
		}
	}

}