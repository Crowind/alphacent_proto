﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Choice", menuName = "ScriptableObjects/Choice")]
public class Choice : ScriptableObject {
	
	[SerializeField]
	public List<Pairing> outcomes = new List<Pairing>();

	public enum Outcome {
		HugeFail,
		Fail,
		Success,
		HugeSuccess
	}

	[Serializable]
	public struct Pairing {
		public Outcome outcome;
		public Effect effect;
	}
}