using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RiskManager : MonoBehaviour {

	// outcomecards	
	public Choice.Outcome[] outcomes;
	public GameObject outcomeCard;
	public Vector3 startPoint;
	public Vector3 endPoint;
	
	private List<CardToChose> cards = new List<CardToChose>();
	
	private Queue<Choice> choicesQueue;
	private Choice currentChoice;
	
	
	
	private Player player;
	
	
	private void Awake() {
		Init();
	}
	

	public void Pick(Choice.Outcome outcome) {
		
		List<Choice.Pairing> effects = currentChoice.outcomes.FindAll(c => c.outcome == outcome);

		foreach (Choice.Pairing consequence in effects) {
			consequence.effect.Apply();
		}
		if (choicesQueue.Count > 0) {
			NewChoice();
		}
		else {
			EndEncounter();
		}
	}


	public void Init() {
		for (int i = 0; i < outcomes.Length; i++) {
			GameObject obj = Instantiate(outcomeCard, Vector3.Lerp(startPoint,endPoint, (float)i/(float)outcomes.Length), Quaternion.identity, transform);
			cards.Add(obj.GetComponent<CardToChose>());
			obj.SetActive(false);
		}
	}


	public void StartEncounter(Choice[] choices) {

		if (player == null) {
			player = FindObjectOfType<Player>();
		}
		player.canMove = false;
		choicesQueue = new Queue<Choice>(choices);
		NewChoice();
		
	}

	private void EndEncounter() {
		foreach (CardToChose card in cards) {
			card.gameObject.SetActive(false);
		}
		player.canMove = true;
	}
	public void NewChoice() {
		currentChoice = choicesQueue.Dequeue();
		System.Random rnd = new System.Random();
		outcomes = outcomes.OrderBy(x => rnd.Next()).ToArray();
		for (int i = 0; i < cards.Count; i++) {
			var card = cards[i];
			card.gameObject.SetActive(true);
			card.outcome = outcomes[i];
		}
	}
	
}